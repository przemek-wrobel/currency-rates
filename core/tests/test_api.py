from django.test import TestCase


class GetAPITest(TestCase):
    """
    Testing Browsbale API entrance point
    """

    def test_get_browsable_api(self):
        """
        Testing GET method on browsable api root
        """

        response = self.client.get('/api/v1/')
        self.assertEqual(response.status_code, 200)

    def test_get_api_json_format(self):
        """
        Testing GET method on api root in json format
        """

        response = self.client.get('/api/v1/?format=json')
        self.assertEqual(response.status_code, 200)

        # TODO add more asserts testing response content
